---
title: "À propos de moi"
date: 2021-05-24T21:49:13+02:00
draft: false
---

Bonjour ! Je m’appelle Benoît, né en Belgique en 1985 et installé au Canada depuis septembre 2021. Curieux de nature et passionné par les voyages et les découvertes, je suis aussi développeur informatique, une vocation qui mêle passion et formation.

Mon parcours professionnel a débuté dans un Centre Hospitalier Universitaire, où j’ai travaillé pendant 13 ans comme analyste programmeur. Cette expérience m’a permis de concevoir, développer et intégrer des applications médicales en utilisant une large palette de technologies.

Au fil des années, j’ai eu l’opportunité d’approfondir mes compétences dans des domaines variés comme l’administration de serveurs et le support technique, renforçant ainsi ma polyvalence dans le secteur informatique.

En parallèle, mon engagement en tant que bénévole et référent informatique dans une Société Protectrice des Animaux reflète mon désir de mettre mes compétences au service de causes qui me tiennent à cœur.

Sur ce blog, je partage mes expériences, les défis que j’ai rencontrés et les solutions que j’ai mises en place. Mon objectif est d’offrir des ressources utiles à d’autres passionnés ou professionnels de l’informatique.

## Technologies et compétences

Voici un aperçu des technologies et outils que j’ai utilisés dans le cadre de différents projets :

**Développement :**

- **Langages et frameworks :** HTML, CSS, JavaScript, Java (Spring Boot, Java EE, JavaServer Pages), SQL, Python (Django Framework, Beautiful Soup), PHP
- **Outils et plateformes :** Atlassian Confluence, Atlassian Jira, WordPress, Xcode/iOS
- **API et protocoles :** REST API, SOAP Web Services

**Systèmes :**

- Microsoft Windows (XP à 10) et Windows Server
- Apache Tomcat, Apache HTTP Server, NGINX
- Active Directory
- Bases de données : SQL Server, Sybase, MySQL, PostgreSQL
- Systèmes Linux : Debian/Ubuntu, CentOS/Red Hat

**Technologies médicales :**

- Protocoles : Health Language 7 (HL7)
- Logiciels et bases de données : InterSystems Caché, InterSystems HealthShare, Zorgi Care
