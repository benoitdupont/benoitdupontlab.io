+++
draft = false
date = 2021-12-08T10:51:28-04:00
title = "Django - se connecter avec l'adresse email"
description = "Utiliser l'email au lieu du nom d'utilisateur pour se connecter à Django"
slug = ""
authors = []
tags = ["python", "django", "authentication"]
categories = []
externalLink = ""
series = []
+++

Django propose de créer un nom d'utilisateur pour l'authentification.
Pour mon projet, je souhaite que la personne ne doive pas retenir un nom d'utilisateur, mais simplement utiliser son adresse email.
Django dispose déjà d'un champ `adresse électronique` au niveau de son objet `User`, j'ai donc cherché à le réutiliser pour l'authentification.

Lors de mes recherches sur Google, je suis tombé sur des articles qui proposent de réécrire une partie du backend d'authentification.
Mon approche est beaucoup plus simple, cacher le champ `nom d'utilisateur` dans les formulaires et copier l'adresse email dans ce champ lors de la sauvegarde du formulaire.


Voici un exemple du code que j'ai implémenté.

```python
from django.contrib.auth.forms import UserCreationForm
# modéle de user custom mais fonctionne aussi avec le User d'origine de Django
from users.models import User


class UserRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        # ne pas afficher le champ nom d'utilisateur à l'écran.
        fields = ("email", "first_name", "last_name")

    def save(self, commit=True):
        user = super().save(commit=False)
        # copier la valeur du champ email dans le champ username.
        user.username = self.cleaned_data.get('email')
        if commit:
            user.save()
        return user
```