+++ 
draft = true
date = 2021-06-01T16:52:51+02:00
title = "WordPress : exporter articles et images d'un domaine à un autre"
description = ""
slug = ""
authors = []
tags = ["wordpress"]
categories = []
externalLink = ""
series = []
+++

WordPress est facile d'utilisation pour créer du contenu.
Il est par contre plus complexe à manipuler lorsqu'il s'agit de procéder à des modifications techniques.

Un projet m'a conduit à récupérer les articles d'un site vers un autre.
Le client souhaitait créer un nouveau site sous un nouveau nom de domaine mais récupérer uniquement les articles de l'ancien site. 

WordPress permet d'exporter (sans plugin) le contenu mais ce n'est pas aussi simple.
Lors de la génération du fichier d'exportation, seuls les contenus "textes" sont exportés. 
Les fichiers images, vidéos et autres de la bibiothèque de médias ne font pas partie de cette exportation, même si "médias" est sélectionné dans les options.

WordPress exporte uniquement les références vers l'endroit où se trouve physiquement le fichier sur le serveur.


Lors de l'exportation il faut penser à copier sur le nouveau serveur tout le répertoire ```wp-content/uploads``` du serveur sur lequel se trouve l'ancien contenu.
Après cette manipulation, tous les fichiers seront sur le nouveau serveurs mais pas connus de WordPress.
Pour celà il faut *exporter les médias*. Chaque référence connue de WordPress sera ainsi connue également sur le nouveau serveur. 


- Copier le répertoire wp-content/uploads de l'ancien vers le nouveau serveur
- Exporter les médias (Outils > Exporter > Médias) de l'ancien site
- Importer les médias (Outils > Importer) sur le nouveau site
- Exporter les articles (Outils > Exporter > Articles) de l'ancien site
- Importer les articles (Outils > Importer) sur le nouveau site
- Exécuter la commande de remplacement de nom de domaine


```bash
wp search-replace 'https://www.anciendomaine.be' 'https://www.nouveaudomaine.be/'
```