+++ 
draft = false
date = 2021-07-05T12:46:39+02:00
title = "Redirection de port sur IIS"
description = "Redirigier le port 80 vers un autre port sur le même serveur."
slug = ""
authors = []
tags = ["iis","serveur"]
categories = []
externalLink = ""
series = []
+++


Surprise lors de la mise en place d'un nouveau projet.
L'application d'un fournisseur doit communiquer avec un service REST sur le port 57772.
Mais dans la configuration de ce logiciel seuls les ports inférieurs à 9999 sont autorisés.

Il n'était pas possible de reconfigurer entièrement notre API REST sur un autre port.
Nous nous sommes orientés vers de la redirection de port afin de rendre possible la communication entre les deux systèmes.

Nous allons rediriger le port 80 vers le port 57772 grâce au IIS sur lequel tourne le service REST qui doit être appelé par le fournisseur.
 

Sur le serveur IIS :

1. [Télécharger et installer le URL Rewrite de IIS](https://www.iis.net/downloads/microsoft/url-rewrite#additionalDownloads)
2. [Télécharger et installer le request routing de IIS](https://www.iis.net/downloads/microsoft/application-request-routing#additionalDownloads)
3. Ouvrir le IIS Manager
4. Se rendre dans *Sites > Default Web Site*
5. Cliquer sur *URL Rewrite*
6. Clic-droit > *Add Rule*
7. Choisir *Blank Rule*
8. Donner un nom à la rule
9. Dans Match URL choisir *Matches the Pattern*
10. Dans Using choisir *Regular Expressions*
11. Dans Pattern mettre *(.\*)* et cocher *ignore case*
12. Dans Action choisir le type *Rewrite*
13. Mettre Rewrite URL avec la valeur vers le serveur à atteindre : http://target-server.domain.com:57772/{R:0}
14. Cocher *Append query string*
15. Sauver la règle
16. Se rendre dans *Application Request Routing Cache*
17. Cliquer sur *Server Proxy Settings...*
18. Cocher *Enable Proxy*


Exemple de configuration :

![iis exemple configuration port mapping](images/iis-redirection-port.png)

![iis exemple configuration port mapping](images/iis-redirection-port-proxy.png)
