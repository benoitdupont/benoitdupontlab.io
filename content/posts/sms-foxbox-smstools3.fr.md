+++ 
draft = true
date = 2021-06-22T12:55:43+02:00
title = "Envoi de SMS avec FoxBox et smstools3"
description = ""
slug = ""
authors = []
tags = ["python", "sms"]
categories = []
externalLink = ""
series = []
+++

FoxBox est un boitier qui sert de passerelle pour l'envoi et la réception de SMS.
Contairement à des opérateurs "dans le cloud", FoxBox utilise des cartes SIM comme dans un smartphone.

Notre société (ou un particulier) peu dés-lors négocier le tarif de l'abonnement SMS associé à la carte SIM utilisée dans l'appareil.

FoxBox est basé sur un système d'exploitation Debian Linux 10 qui exécute les logiciels open source [smstools3](http://smstools3.kekekasvi.com/) et [playSMS](https://playsms.org/about/).


# Fonctionnalités

D'après le vendeur, FoxBox dispose des fonctionnalités suivantes :

* Envoi réception de SMS/MMS.
* Passerelle de email-> SMS et SMS->email.
* Statistiques sur les SMS.
* Effectuer des actions sur les SMS envoyés ou reçus (par exemple une personne qui répond correctement à un jeu).
* ...

Comme précisé ci-dessus, FoxBox inclut l'application open source [playSMS](https://playsms.org/about/) et hérite de ce fait de toutes ses fonctionnalités.


Seul l'envoi et la réception automatisé de SMS sont implémentés dans le cadre de mon projet.
Les autres fonctionnalités n'ont pas été testées. 


# Envoi/réception de SMS à partir d'une application tierce

Le boîtier dispose d'une interface Web pour l'envoi/réception/gestion des SMS.



# Problèmes rencontrés et support technique

## Custom Applications

Dans l'interface Web de FoxBox, il est possible de modifier des scripts pour gérer la réception ou l'envoi des SMS.

**Attention**, utiliser cette interface Web pour éditer les scripts corrompt tout simplement le script.
Des caractères étranges sont ajoutés à chaque ligne du script et le rend inutilisable par le système d'exploitation.

Si vous devez éditer ces scripts, utilisez un "vrai" éditeur de texte.
Les fichiers se situent à ces endroits :
* ```/mnt/flash/root/source/custom/custom_rx```
* ```/mnt/flash/root/source/custom/custom_tx```

## Suppport technique

Inexistant !

Plusieurs questions ont été posées via le formulaire de contact en ligne.
Une seule réponse a été fournie et elle ne répondait pas du tout à la question.
Par la suite, plus aucun signe de vie de la société.

Le système de création de ticket ne semble pas fonctionner, impossible de demander du support en créant un ticket.

La FoxBox n'est qu'un boitier contenant un système d'exploitation Linux et les logiciels [smstools3](http://smstools3.kekekasvi.com/) et [playSMS](https://playsms.org/about/), je vous invite à consulter la documentation et le support de ces logiciels au lieu de perdre du temps avec la société SMS FoxBox.

## Code qui tombe en erreur

Lors de la consultation du log de smstools3, je me suis rendu compte que le code fourni par la société tombe en erreur à chaque message reçu.
Ce problème n'affecte pas la manière dont j'utilise la box mais je ne sais pas quels autres impacts ça aura pour une utilisation complète des fonctionnalités de FoxBox.

Encore une fois, le support de FoxBox a été averti mais nous n'avons jamais reçu de réponse. 

```
2021-06-22 14:27:58.590,5, GSM3: SMS received, From: 32478908706
2021-06-22 14:27:58.801,3, GSM3: Exec: eventhandler said something:
2021-06-22 14:27:58.801,3, GSM3: ! <BR>
2021-06-22 14:27:58.802,3, GSM3: ! STATUS:
2021-06-22 14:27:58.802,3, GSM3: ! 

2021-06-22 14:27:58.802,3, GSM3: ! :
2021-06-22 14:27:58.802,3, GSM3: ! ------
2021-06-22 14:27:58.802,3, GSM3: ! 

2021-06-22 14:27:58.802,3, GSM3: ! INSERT INTO smsin (id,mittente,smsc,spedito,ricevuto,sogg,alfabet,testo,tms,stato) values (NULL,'From: 32478908706','From_SMSC: 32495002530','Sent: 21-06-22 14:27:55','Received: 21-06-22 14:27:58','new','Alphabet: UTF-8','IMEI: 863789027073422Test','1624364878','32478908706');
2021-06-22 14:27:58.802,3, GSM3: ! 

2021-06-22 14:27:58.802,3, GSM3: ! Number of rows modified: 1beep: Error: Could not open any device
2021-06-22 14:27:58.804,3, GSM3: Exec: eventhandler encountered errors:
2021-06-22 14:27:58.804,3, GSM3: ! PHP Notice:  Undefined variable: response in /etc/sms/scripts/parserGSM3.php on line 249
```

Vous pouvez retrouver les logs dans ```/var/log/smstools/smsd.log```