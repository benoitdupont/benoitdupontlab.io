+++ 
draft = true
date = 2021-05-31T16:27:39+02:00
title = "Mon aventure à la SPA La Louvière"
description = ""
slug = ""
authors = []
tags = ["bénévole"]
categories = []
externalLink = ""
series = []
+++

Depuis 2019, j'ai la chance de faire partie de la super équipe de la SPA La Louvière en tant que bénévole.

Ce n'est pas auprès des animaux que j'apporte mon aide, mais plutôt dans l'évolution du site web et le développement d'un logiciel interne à la gestion des animaux du refuge.

Je me suis retrouvé dans cette aventure car j'avais en tête de développer une application pour déclarer des animaux perdus.
Je venais à ce moment de perdre mon chien (heureusement retrouvé rapidement).
Une discussion avec le président du refuge a permis de déduire qu'une telle application n'est pas utile si elle n'est pas connue du grand publique.
De plus, d'autres applications du même type existent déjà.


Afin d'aider le refuge, j'ai proposé mon aide dans un domaine que je connais le mieux, l'informatique.
Ma première mission fut de faire évoluer le site web.






 Parler de la stack 






Dev manager (faire un article que sur le manager et le présenter comme tag projet + screenshots)

Création nouveau site web (idem faire un article avec les choix et les complexités)

Parler de l’aventure OVH qui a brûlé et du backup sur Git



## Le site web




## L'outils de gestion
