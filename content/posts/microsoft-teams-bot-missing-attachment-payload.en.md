+++
draft = false
date = 2024-12-17T16:51:28-04:00
title = "Microsoft Teams Bot - Resolving Missing Attachments in Azure Bot Service Payload"
description = "How to work around missing attachments in Azure Bot Service payloads from Microsoft Teams when processing shared files."
slug = ""
authors = []
tags = ["microsoft-teams", "microsoft", "azure", "azure-bot-service", "microsoft-graph-api"]
categories = []
externalLink = ""
series = []
+++

At my company, we have a Microsoft Teams bot that can send faxes from a file in your chat conversation.

Over the past few months, we've encountered an issue with Microsoft because the payload we receive from Microsoft Teams to our server is missing some important data, the attachments. This issue occurs specifically when a user attempts to send a fax using a file shared by another colleague.

Usually, the payload from Microsoft Azure Bot Services looks like the following code:

```json
{
  "attachments": [
    {
      "contentType": "application/vnd.microsoft.teams.file.download.info",
      "content": {
        "downloadUrl": "https://xxwdq-my.sharepoint.com/personal/adm_xxwdq_onmicrosoft_com/_layouts/15/download.aspx?UniqueId=e83f939b-e906-4bbd-b9bd-f23c5e3fd400&Translate=false&tempauth=v1.eyJzaXRlaWQiOiJjZmYwMjE5Zi1kNDdmLTRkNGEtODdkMy02NzViZWY0MTMzMDQiLCJhcHBfZGlzcGxheW5hbWUiOiJNaWNyb3NvZnQgVGVhbXMgU2VydmljZXMiLCJhcHBpZCI6ImNjMTVmZDU3LTJjNmMtNDExNy1hODhjLTgzYjFkNTZiNGJiZSIsImF1ZCI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMC94eHdkcS1teS5zaGFyZXBvaW50LmNvbUBjYWFmZWYxMC05ZDEyLTQ2NjgtYTBkNi0yNjhiNzE2OTVlMWEiLCJleHAiOiIxNzM0MTg3MDgzIn0.CgoKBHNuaWQSAjY0EgsI1Km9kdKTzj0QBRoNMjAuMTkwLjEzMi40MSosaUN6TWQ5bWVMTXNIMkYvSWFWUTEyQlN2bkpMSFc0clFEZndqMk9uV2t5Zz0wmgE4AUIQoW15DP3QAHBB4Tssnamh-0oQaGFzaGVkcHJvb2Z0b2tlbnIpMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZmI0NzUxYzJAbGl2ZS5jb216ATKCARIJEO-vyhKdaEYRoNYmi3FpXhqSAQxCZW5vaXQgMTE6MDeaAQ1EdXBvbnQgIDExOjA3ogEZYWRtQHh4d2RxLm9ubWljcm9zb2Z0LmNvbaoBEDEwMDMyMDAxRkI0NzUxQzKyASxncm91cC53cml0ZSBhbGxzaXRlcy53cml0ZSBhbGxwcm9maWxlcy53cml0ZcgBAQ.aNQDbSXn9lhjbM9rEn_m3OHSr71bGITt0k_WweVqhyI&ApiVersion=2.0",
        "uniqueId": "e83f939b-e906-4bbd-b9bd-f23c5e3fd400",
        "fileType": "pdf"
      },
      "contentUrl": "https://xxwdq-my.sharepoint.com/personal/adm_xxwdq_onmicrosoft_com/Documents/Microsoft Teams Chat Files/demo 14.pdf",
      "name": "demo 14.pdf"
    },
    {
      "contentType": "text/html",
      "content": ""
    }
  ],
  "type": "message",
  "timestamp": "2024-12-14T13:38:03.752379Z",
  "localTimestamp": "2024-12-14T09:38:03.752379-04:00",
  "id": "1734183483728",
  "channelId": "msteams",
  "serviceUrl": "https://smba.trafficmanager.net/amer/caafef10-9d12-4668-a0d6-268b71695e1a/",
  "from": {
    "id": "29:1JiEH1nLmzqH5YT7Do_1KRCs7ISDxUmVVKPbIzwM1pc884QpFbzi7GFznE58JZKe5sdjw65yzjAZtxyJt55B3mA",
    "name": "Benoit Dupont",
    "aadObjectId": "af24b8a2-cfe8-453a-a9b4-4a917b1534ae"
  },
  "conversation": {
    "conversationType": "personal",
    "tenantId": "caafef10-9d12-4668-a0d6-268b71695e1a",
    "id": "a:1ktMfOkUqtyhd4ub9GhZwaOdqpjjygWz5IZGvHZLTCnduBU1HmsFGO9vBAMZMQhqJOeiphjhE_k9G3HerhrBgeIf6uuA-FYRX93w_OsJYVTFX1lB36Q5xJgAGJtizZM1o"
  },
  "recipient": {
    "id": "28:45fc30f1-94c5-48aa-b0df-ef0e41b65c6c",
    "name": "Core Fax"
  },
  "entities": [
    {
      "locale": "en-US",
      "country": "US",
      "platform": "Web",
      "timezone": "America/Halifax",
      "type": "clientInfo"
    }
  ],
  "channelData": {
    "tenant": {
      "id": "caafef10-9d12-4668-a0d6-268b71695e1a"
    }
  },
  "locale": "en-US",
  "localTimezone": "America/Halifax"
}
```

We can clearly see the file the user want to send by fax. We can grab that file and implement code to send it.

When the file is shared from someone else, the payload is kind of empty like this:

```json
{
  "text": "send fax to 11111",
  "textFormat": "plain",
  "attachments": [
    {
      "contentType": "text/html",
      "content": "<p>send fax to 11111</p>"
    }
  ],
  "type": "message",
  "timestamp": "2024-12-14T13:11:52.7298765Z",
  "localTimestamp": "2024-12-14T09:11:52.7298765-04:00",
  "id": "1734181912704",
  "channelId": "msteams",
  "serviceUrl": "https://smba.trafficmanager.net/amer/caafef10-9d12-4668-a0d6-268b71695e1a/",
  "from": {
    "id": "29:1JiEH1nLmzqH5YT7Do_1KRCs7ISDxUmVVKPbIzwM1pc884QpFbzi7GFznE58JZKe5sdjw65yzjAZtxyJt55B3mA",
    "name": "Benoit Dupont",
    "aadObjectId": "af24b8a2-cfe8-453a-a9b4-4a917b1534ae"
  },
  "conversation": {
    "conversationType": "personal",
    "tenantId": "caafef10-9d12-4668-a0d6-268b71695e1a",
    "id": "a:1ktMfOkUqtyhd4ub9GhZwaOdqpjjygWz5IZGvHZLTCnduBU1HmsFGO9vBAMZMQhqJOeiphjhE_k9G3HerhrBgeIf6uuA-FYRX93w_OsJYVTFX1lB36Q5xJgAGJtizZM1o"
  },
  "recipient": {
    "id": "28:45fc30f1-94c5-48aa-b0df-ef0e41b65c6c",
    "name": "Core Fax"
  },
  "entities": [
    {
      "locale": "en-US",
      "country": "US",
      "platform": "Web",
      "timezone": "America/Halifax",
      "type": "clientInfo"
    }
  ],
  "channelData": {
    "tenant": {
      "id": "caafef10-9d12-4668-a0d6-268b71695e1a"
    }
  },
  "locale": "en-US",
  "localTimezone": "America/Halifax"
}
```

This use case used to work for the last past few years, but suddenly doesn't work anymore.

Our app already have access to Microsoft Graph to get some other data and Microsoft Graph has endpoints to access the messages from a Microsoft Teams conversation.

We investigated whether Microsoft Graph could provide the missing attachment information and missing attachments from the original Microsoft Teams payload.

## Workaround solution

### Getting the `chatId` from the Microsoft Bot Service payload

It's easy to read a Microsoft Teams message using Microsoft Graph.

The other issue is to find the appropriate IDs to make the call to the following endpoint `https://graph.microsoft.com/v1.0/chats/{chatId}/messages/{messageId}` (see [Microsoft documentation](https://learn.microsoft.com/en-us/graph/api/chatmessage-get?view=graph-rest-1.0&tabs=http))

- `{messageId}`: is the payload's `id` value.
- `{chatId}`: nowhere to be found in the payload. We need more Microsoft Graph API calls to find out the right value.

Microsoft have documentation to get it, but [these samples](https://learn.microsoft.com/en-us/microsoftteams/platform/graph-api/proactive-bots-and-messages/graph-proactive-bots-and-messages?tabs=dotnet#retrieve-the-conversation-chatid) didn't work.

That sample `GET https://graph.microsoft.com/v1.0/users/{user-id}/chats?$filter=installedApps/any(a:a/teamsApp/id eq '{teamsAppId}')` is not working, but close to what I need.

I managed to make it work with the following query:

`GET https://graph.microsoft.com/v1.0/users/{user-id}/chats?filter=contains(id,'{recipient-id}')`.

- `{user-id}`: value from `payload.from.aadObjectId`
- `{recipient-id}`: value from `payload.recipient.id`, with a twist. We need to remove the part before the `:`

It gets the user's chat list and finds the one containing the recipient (our bot) ID.
A `chatId` is composed of both user and recipient IDs.

The result looks like this:

```json
{
  "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#chats",
  "@odata.count": 1,
  "@odata.nextLink": "https://graph.microsoft.com/v1.0/users/af24b8a2-cfe8-453a-a9b4-4a917b1534ae/chats?filter=contains(id%2c%2745fc30f1-94c5-48aa-b0df-ef0e41b65c6c%27)&$skiptoken=1.kscDYs0BEcYAAAEU8P8DgapQcm9wZXJ0aWVzgqlTeW5jU3RhdGXZ6GV5SmtaV3hwZG1WeVpXUlRaV2R0Wlc1MGN5STZXM3NpYzNSaGNuUWlPaUl5TURJeUxUQTVMVEkyVkRFeE9qVXdPak0yTGpFek1Tc3dNRG93TUNJc0ltVnVaQ0k2SWpJd01qUXRNVEl0TVRSVU1USTZNekU2TWprdU16ZzFLekF3T2pBd0luMWRMQ0o2WlhKdlRFMVRWRVJsYkdsMlpYSmxaRk5sWjIxbGJuUnpJanBiWFN3aWMyOXlkRTl5WkdWeUlqb3dMQ0pwYm1Oc2RXUmxXbVZ5YjB4TlUxUWlPbVpoYkhObGZRPT2sTGFzdFBhZ2VTaXplojIw",
  "value": [
    {
      "id": "19:af24b8a2-cfe8-453a-a9b4-4a917b1534ae_45fc30f1-94c5-48aa-b0df-ef0e41b65c6c@unq.gbl.spaces",
      "topic": null,
      "createdDateTime": "2024-11-29T13:10:05.361Z",
      "lastUpdatedDateTime": "2024-11-29T13:37:52.869Z",
      "chatType": "oneOnOne",
      "webUrl": "https://teams.microsoft.com/l/chat/19%3Aaf24b8a2-cfe8-453a-a9b4-4a917b1534ae_45fc30f1-94c5-48aa-b0df-ef0e41b65c6c%40unq.gbl.spaces/0?tenantId=caafef10-9d12-4668-a0d6-268b71695e1a",
      "tenantId": "caafef10-9d12-4668-a0d6-268b71695e1a",
      "onlineMeetingInfo": null,
      "viewpoint": {
        "isHidden": false,
        "lastMessageReadDateTime": "2024-12-14T12:31:27.209Z"
      }
    }
  ]
}
```

### Obtain the message from Microsoft Graph

Now that we have the `value.id`, we can make a call to the message endpoint:

`https://graph.microsoft.com/v1.0/chats/{chatId}/messages/{messageId}`

The result gives us what we needed in the first place, the attachments :)

```json
{
  "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#users('af24b8a2-cfe8-453a-a9b4-4a917b1534ae')/chats('19%3Aaf24b8a2-cfe8-453a-a9b4-4a917b1534ae_45fc30f1-94c5-48aa-b0df-ef0e41b65c6c%40unq.gbl.spaces')/messages/$entity",
  "id": "1734119051027",
  "replyToId": null,
  "etag": "1734119051027",
  "messageType": "message",
  "createdDateTime": "2024-12-13T19:44:11.027Z",
  "lastModifiedDateTime": "2024-12-13T19:44:11.027Z",
  "lastEditedDateTime": null,
  "deletedDateTime": null,
  "subject": null,
  "summary": null,
  "chatId": "19:af24b8a2-cfe8-453a-a9b4-4a917b1534ae_45fc30f1-94c5-48aa-b0df-ef0e41b65c6c@unq.gbl.spaces",
  "importance": "normal",
  "locale": "en-us",
  "webUrl": null,
  "channelIdentity": null,
  "policyViolation": null,
  "eventDetail": null,
  "from": {
    "application": null,
    "device": null,
    "user": {
      "@odata.type": "#microsoft.graph.teamworkUserIdentity",
      "id": "af24b8a2-cfe8-453a-a9b4-4a917b1534ae",
      "displayName": "Benoit Dupont",
      "userIdentityType": "aadUser",
      "tenantId": "caafef10-9d12-4668-a0d6-268b71695e1a"
    }
  },
  "body": {
    "contentType": "html",
    "content": "<p>send fax to 1111</p><attachment id=\"0b922164-a59f-44bc-8a4d-a94ad77bfc\"></attachment>"
  },
  "attachments": [
    {
      "id": "0b922164-a59f-44bc-8a4d-a94ad77bfc",
      "contentType": "reference",
      "contentUrl": "https://xxwdq-my.sharepoint.com/personal/_xxwdq_onmicrosoft_com/Documents/Microsoft%20Teams%20Chat%20Files/PDF.PDF",
      "content": null,
      "name": "PDF.PDF",
      "thumbnailUrl": null,
      "teamsAppId": null
    }
  ],
  "mentions": [],
  "reactions": []
}
```

This workaround helps to leverage Microsoft Graph API to retrieve missing attachment data in Azure Bot Service payloads for Microsoft Teams.
This approach ensures uninterrupted service for users relying on our bot for faxing shared files.
