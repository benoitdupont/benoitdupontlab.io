+++
draft = false
date = 2025-01-19T15:01:28-04:00
title = "Xcode kvstore-identifier Issue After App Ownership Transfer"
description = "How to resolve the Xcode kvstore-identifier entitlement error caused by Apple iOS app ownership transfer."
slug = ""
authors = []
tags = [""]
categories = []
externalLink = ""
series = []
+++

The other day, we needed to transfer the ownership of our iOS apps from our old Apple Developer account to the new company account.

Doing so resulted in an error on the Xcode Signing Certificate status screen:

```
Provisioning profile "iOS Team Provisioning Profile: com.xxx.xxx" doesn't match the entitlements file's value for the com.apple.developer.ubiquity-kvstore-identifier entitlement.
```

![Xcode ubiquity-kvstore-identifier entitlement issue](images/xcode-signing-certificate-kvstore-identifier-issue.png)

This issue is linked to the use of the iCloud Key-Value storage service.

By default, the `.entitlements` configuration file uses Xcode variables for the `kvstore-identifier` value, as shown below:

```bash
com.apple.developer.ubiquity-kvstore-identifier = $(TeamIdentifierPrefix)$(CFBundleIdentifier)
```

When the account owner changes, the Team ID and the `$(TeamIdentifierPrefix)` value also change accordingly. However, the `kvstore-identifier` name on Apple's servers does not update automatically and continues to use the old Team ID as its prefix. This name is not modified during the ownership transfer process.

To validate this theory, you can use the following command to inspect the provisioning profile:

```bash
security cms -D -i ~/Library/Developer/Xcode/UserData/Provisioning\ Profiles/xxxx.mobileprovision
```

This command decodes and displays the `.mobileprovision` file to allow you to confirm the `kvstore-identifier` value Apple is expecting you to use in the `.entitlements` file.

Note: The path to the `.mobileprovision` file is for Xcode 16 and is different for older Xcode version.

---

References:

- https://forums.developer.apple.com/forums/thread/730466
- https://stackoverflow.com/questions/26363592/tool-for-displaying-the-contents-of-a-provisioning-profile-mobileprovision-fi
- https://github.com/xamarin/xamarin-macios/issues/20771
- https://developer.apple.com/help/app-store-connect/transfer-an-app/overview-of-app-transfer/
