+++ 
draft = false
date = 2021-07-18T16:06:51+02:00
title = "Le prix excessif d'un nom de domaine"
description = "Un nom de domaine peut s'acheter de un à plusieurs miliers d'euros."
slug = ""
authors = []
tags = []
categories = ["hébergement", "dns"]
externalLink = ""
series = []
+++

Lancer un site web sur Internet commence par décider du nom de domaine que l'on va utiliser.
Un nom de domaine est l'adresse Internet que vous entrez dans le navigateur web pour vous rendre sur vos sites favoris.
Vous êtes actuellement en train de me lire à partir du nom de domaine be-dupont.com

Pour pouvoir être utilisé, un nom de domaine doit s'acheter auprès de compagnies spécialisées et accréditées tel que www.ovh.com (rubrique Web Cloud > Noms de domaine.)


La première idée de nom de site web fut dben.com.
Je me suis joyeusement rendu sur le site www.dben.com pour savoir si ce domaine était libre.
Malheureusment, je suis tombé sur la page suivante

![dben.com déjà acheté par quelqu'un d'autre](images/dben-com.png)

Quelqu'un d'autre est déjà propriétaire du domaine (et l'est toujours au moment où je rédige ces lignes).
Le site indique que le nom de domaine est à vendre et qu'il faut contacter une adresse e-mail afin de connaître le prix de vente.
Je m'exécute et demande le montant de ce nom de domaine qui me plaît tant. 

Le lendemain, une réponse attend dans ma boîte mail. Voici son contenu.


> Thank you for your email.
>
> The domain name is currently priced at $33,000 USD.  This is a single one-time-only payment.  This will give you complete ownership and control of the domain name, and all rights to its future use.
>
> This is a lower price than the following 3 comparable domain names sold:\
> fbet.com - Sold for $55,392 USD - 5/20/2015\
> oben.com - Sold for $50,000 USD - 7/31/2015\
> kbet.com - Sold for $43,530 USD - 3/18/2019
>
> For your peace of mind, the purchase transaction is guaranteed through the online escrow service at Escrow.com\
> The seller pays for the basic escrow fee (for a wire transfer at Escrow.com).\
> Escrow.com only releases your payment to us after the domain name has been properly transferred.\
> Escrow.com's domain name escrow service is further explained at:
> https://www.escrow.com/domains
>
> After payment, you will be able to make future changes to the domain name registration yourself.
>
> Aswell as the immediate value added to your website, a good short dot com domain name can be an excellent investment, as this Wikipedia page shows, listing over 30 domains which have sold for $3 million or more (e.g. tesla.com sold for $11 million):
> https://en.wikipedia.org/wiki/List_of_most_expensive_domain_names
>
> Please email me if you would like to proceed, if you would like to make an offer, or if you have any questions?   I look forward to hearing from you.


En deux mots, cela signifie que le prix demandé pour le nom de domaine dben.com est de 33000$. Oui oui vous avez bien lu.
Ce tarif super élevé s'appuie sur le nombre de lettres qui composent le nom.
En règle générale, les noms très courts sont les plus prisés, car ils sont plus faciles à mémoriser pour l'être humain.

Imaginez une publicité avec une adresse de site web www.moi.com comparé au nom www.moijesuistropbiendansmapeau.com.
Lequel des deux avez-vous le plus de chance d'avoir retenu d'ici demain ? Le plus court bien évidemment.

On se rend compte avec la proposition précédente que des noms de domaine courts et faciles à retenir sont achetés à bas prix dans le but de les revendre plus cher au plus offrant.
Imaginez le domaine dben.com acheté il y a quelques années à 20€/an (prix moyen d'un nom de domaine) et le revendre au plus offrant aux alentours de 33000€. Vous pouvez d'ailleurs trouver une liste des noms de domaine les plus chers sur [Wikipédia](https://en.wikipedia.org/wiki/List_of_most_expensive_domain_names).

Après plusieurs heures de réflexion et de créativité, mon cerveau est tombé sur le nom be-dupont.com qui, au final, reflète mieux ce que représente ce site/blog qui a le but de raconter ma vie et mes expériences "être Dupont".
