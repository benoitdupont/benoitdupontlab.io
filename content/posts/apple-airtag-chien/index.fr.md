+++ 
draft = false
date = 2021-08-01T12:42:35+02:00
title = "Un Apple AirTag pour localiser mon chien"
description = ""
slug = ""
authors = []
tags = ["gps", "animal", "géolocalisation", "chien", "apple", "airtag"]
categories = []
externalLink = ""
series = []
+++


Depuis la fugue de mon chien en 2016, j'utilise un Tracker GPS pour localiser mon fidèle compagnon en cas de besoin.

Début 2021, Apple lance l'AirTag, petit boîtier connecté utilisé pour retrouver facilement des objets égarés.
Est-ce un bon plan pour mon animal ?


# Un Tracker GPS, c'est quoi ?

Un GPS tracker est un "petit appareil" (pas si petit que ça en fait) à mettre autour du cou de votre compagnon.
De longues recherches m'ont menées sur le GPS Tracker *Kippy* (première génération).
À cette époque, les avis utilisateurs le recommandent pour la fiabilité de la localisation et une batterie qui reste chargée plus longtemps que les produits concurrents.

Lors de l'utilisation du Kippy, je me suis rendu compte que la localisation prend à-peu-près dix minutes pour trouver la position de l'animal.
Il y a même des chances que l'animal ne soit jamais trouvé s'il est à l'intérieur d'une habitation ou trop près de plusieurs murs.
Typiquement la cour d'une habitation.
Je pense que ce serait le même problème dans les rues d'une grande ville où le signal GPS a du mal à localiser la position.

Un an plus tard, je me vois obligé d'acheter une nouvelle version de l'appareil.
Je dispose à présent du Kippy Vita.
Comme son grand-frère, il ne localise pas plus précisément ni plus rapidement mon animal.
La batterie ne tient pas plus longtemps non plus.
Il faut la recharger au moins tous les deux jours.

Bref, je ne suis pas satisfait du produit, mais c'était la seule manière d'espérer localiser mon chien en cas de future nouvelle fugue.
En avril 2021, Apple annonce un produit qui pourrait mieux correspondre à mon besoin ; l'Apple AirTag.


![Kippy Vita](images/dimensioni-kippy-vita-500.png)




# Qu'est-ce que l'Apple AirTag ?

L'Apple AirTag est une petite pastille légèrement plus grande qu'une pièce de deux euros.
Elle a le pouvoir de transmettre sa localisation exacte à tous vos appareil de la marque Apple (iPhone, iPad, mac...).
Il vous suffit de l'accrocher à vos clés, votre valise ou tout autre objet que vous avez l'habitude de perdre (un chien ?) pour les retrouver facilement à l'aide de l'application *Localiser*.

L'AirTag s'appuie sur les autres produits Apple à proximité pour déterminer et communiquer sa position.
Imaginez avoir perdu votre valise dans une gare.
L'appareil va notifier les iPhone et iPad d'autres personnes aux alentours de sa présence.
Ces mobiles vont par la suite relayer cette information vers les serveurs de Apple et, enfin, afficher la position exacte de l'AirTag sur votre propre iPhone.
Tout ceci de manière transparente sans que personne n'a accès à ces informations privées à part vous, détenteur de cet AirTag. 

![Apple AirTag](images/tile_keychain__csyd7pmtexci_large.jpg)



# AirTag face au tracker GPS

Le GPS Tracker comme son nom l'indique se base sur la position GPS pour indiquer où se situe l'animal.
Dans la version (ancienne) dont je dispose, il faut une bonne couverture GPS pour que le Kippy capte les satelittes et trouver sa position.
Il est nécessaire aussi d'avoir du réseau mobile afin que le tracker transmette sa position par Internet à l'utilisateur.

Le AirTag quant à lui n'a pas besoin de connexion Internet ni de réception GPS.
Il lui faut surtout croiser une personne ayant un iPhone en sa possession afin de servir de relais entre le AirTag et les serveurs de Apple.

Le GPS Tracker mieux correspondre à un usage extérieur dans la nature alors que l'AirTag sera plus utile dans un milieu urbain.




# Avis après un mois d'utilisation du AirTag


Le AirTag fait son job et arrive à localiser efficacement mon animal.
Cependant, il y a un gros problème pratique lors de l'utilisation ; un AirTag *appartient* à une seule personne.
Mon mari ayant lié le AirTag avec son propre compte Apple, je me retrouve à avoir des alertes qui s'affichent sur mon iPhone lorsque je promène le chien.

À la base, ceci a été pensé par Apple pour qu'un AirTag ne permette pas de suivre une personne à son insu, question de respect de la vie privée.
En pratique c'est assez enuyant. 

L'AirTag étant lié à un seul compte Apple ne permet pas non plus à un autre membre de la famille de localiser cet AirTag.
Seul le propriétaire de l'AirTag sait le localiser, même si les différents comptes Apple sont liés par un *partage familial*.

![Alerte AirTag Apple](images/apple-airtag-iphone-alert-cut.jpg)



# Conclusion


L'AirTag n'est pas la solution ultime que j'espérais à cause de ce problème de "non partage familial".
Lors de la rédaction de ces lignes, je me suis rendu compte que la marque Kippy a lancé un nouveau produit, le Kippy EVO.

Ce dernier se base à présent sur les technologies Bluetooth et WIFI en plus du GPS.
Ceci lui permettrait peut-être de capter plus rapidement et de manière plus fiable sa position.


![Mon chien avec son AirTag](images/kyra-dog-alaskan-malamute-apple-airtag.jpg)